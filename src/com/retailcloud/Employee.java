package com.retailcloud;
import java.io.*;
import java.util.Scanner;

public class Employee {
    public static void main(String[] args) throws FileNotFoundException {
        String name, gender, city;
        int age;
        int flag=1;
        Scanner scanner = new Scanner(System.in);
        File myFile = new File("myFile.txt");
        while(flag==1){
            System.out.print("Enter name : ");
            name=scanner.next();
            System.out.print("Enter age : ");
            age=scanner.nextInt();
            System.out.print("Enter gender : ");
            gender=scanner.next();
            System.out.print("Enter city : ");
            city=scanner.next();

            writeFile(myFile, name,age,gender,city);
            readFile(myFile);

            System.out.print("press 1 if you want to add another...");
            flag= scanner.nextInt();
        }

    }
    public static void writeFile(File myFile, String name,int age,String gender, String city){
        try {
            if(!myFile.exists()){
                myFile.createNewFile();
            }
            System.out.println(name+" "+age+" "+gender+" "+city);
            FileWriter fileWriter = new FileWriter(myFile.getName(),true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("\n\nName : "+name+"\nAge: "+age+"\nGender :"+gender+"\nCity : "+city);
            bufferedWriter.close();
            System.out.println("Successfully wrote to the file.");

        } catch(IOException e) {
            System.out.println("An error occurred.");
        }
    }
    public static void readFile(File myFile) throws FileNotFoundException {
        Scanner scanner = new Scanner(myFile);
        while (scanner.hasNextLine())
            System.out.println(scanner.nextLine());
    }
}
